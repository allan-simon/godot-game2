extends CharacterBody2D

class_name Actor

@export var speed: = Vector2(300.0, 4000.0)
@export var gravity: = 3000.0
