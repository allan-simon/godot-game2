@tool
extends Button

@export_file var next_scene: String = ""


	
func _on_button_up():
	PlayerData.reset() 
	get_tree().change_scene_to_file("res://src/levels/level_template.tscn")
	

func _get_configuration_warnings():
	return ["next_scene_path must be set for the button"] if next_scene == '' else []
