extends Control

@onready var scene_tree: = get_tree()
@onready var pause_overlay: ColorRect = $PauseOverlay 
@onready var score: Label = $Label
@onready var pause_label: Label = $PauseOverlay/Title

var paused: = false : set = set_paused

func _ready():
	PlayerData.score_updated.connect(update_interface)
	PlayerData.player_died.connect(_player_died)
	PlayerData.player_reset.connect(_player_reset)
	update_interface()

func _unhandled_input(event):
	if event.is_action_pressed("pause"):
		self.paused = not paused
		get_viewport().set_input_as_handled()

func set_paused(value: bool) -> void:
	paused = value
	scene_tree.paused = value
	pause_overlay.visible = value

func update_interface():
	score.text = "Score: %s" % PlayerData.score

func _player_died():
	self.paused = true
	pause_label.text = "You died" 

func _player_reset():
	self.paused = false
