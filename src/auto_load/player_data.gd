extends Node

signal score_updated
signal player_died
signal player_reset

var score: = 0 : set = set_score
var deaths: = 0: set = set_deaths

func set_score(new_score: int) -> void: 
	score = new_score
	emit_signal("score_updated")

func set_deaths(new_deaths: int) -> void: 
	deaths = new_deaths
	emit_signal("player_died")


func reset() -> void:
	score = 0
	deaths = 0
	emit_signal("player_reset")
