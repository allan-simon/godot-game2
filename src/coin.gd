extends Area2D

@export var score: = 50

@onready var animation_player: AnimationPlayer = $AnimationPlayer
 


func _on_body_entered(body):
	animation_player.play("fade_out")
	PlayerData.score += score
	pass # Replace with function body.
